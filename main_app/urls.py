from django.urls import path
from . import views


urlpatterns = [
    path('', views.main, name='main'), 
    path('registration/', views.registration, name='registration'),
    path('profile/', views.profile, name='profile'),
    path('films/<int:film_id>/', views.films, name='films'),
    path('films/', views.films, name='films'),
    path('film_adding/', views.film_adding, name='film_adding'),
    path('film_edit/<int:film_id>/', views.film_edit, name='film_edit'),
]
