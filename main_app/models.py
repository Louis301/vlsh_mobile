from django.db import models


class User(models.Model):
    login = models.CharField(max_length=10)
    password = models.CharField(max_length=10)


class Film(models.Model):
    title = models.CharField(max_length=100) 
    year = models.CharField(max_length=10) 
    genre = models.CharField(max_length=50) 
    director = models.CharField(max_length=100) 
    discription = models.CharField(max_length=1000) 
    user_id = models.IntegerField()
    viewed = models.BooleanField(default=False)
