import json


def save_account_info(user_id: int) -> None:
    with open('account_info.json', 'w', encoding='utf-8') as fout:
        json.dump({ 'current_account_id' : user_id }, fout)


def get_current_account_id() -> int:
    with open('account_info.json', 'r', encoding='utf-8') as fin:
        json_data = json.load(fin)
        return json_data.get('current_account_id')
    