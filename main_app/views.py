from django.shortcuts import render
from .models import User, Film
from django.http import HttpResponseRedirect
from .account import *


def main(request):
    msg = ''
    _login = ''
    _password = ''

    if request.POST.get('btn_entering') != None:
        _login = request.POST.get('login')
        _password = request.POST.get('password')

        try:
            current_user = User.objects.get(login=_login, password=_password)
            save_account_info(current_user.id)
            return HttpResponseRedirect('films') 
        
        except User.DoesNotExist:
            msg = 'user not found'

    context = {
        'msg' : msg, 
        'login' : _login,
        'password' : _password,
    }

    return render(request, 'app_start/main.html', context)


def registration(request):
    msg = ''
    _login = ''
    _password = ''

    if request.POST.get('btn_registration') != None:
        _login = request.POST.get('login')
        _password = request.POST.get('password')
        current_user = User.objects.filter(login=_login, password=_password)

        if current_user.count() == 0:
            User.objects.create(login=_login, password=_password)
            return HttpResponseRedirect('/')
        else:
            msg = 'пользователь существует'
    
    context = {
        'msg' : msg, 
        'login' : _login,
        'password' : _password,
    }

    return render(request, 'app_start/registration.html', context)


def films(request, film_id=None):
    films = Film.objects.filter(user_id=get_current_account_id())

    if film_id == None:
        only_one_film = False

        if films.count() == 1:
            only_one_film = True

        context = {
            'films' : films,
            'only_one_film' : only_one_film,
        }
        return render(request, 'app_start/films.html', context)
    
    else:
        new_films_order = []
        only_one_film = False

        films_id_order = [film.id for film in films]
        new_films_id_order = films_id_order[films_id_order.index(film_id):] + films_id_order[:films_id_order.index(film_id)]
        
        for film_index in new_films_id_order:
            new_films_order.append(Film.objects.get(id=film_index))

        if films.count() == 1:
            only_one_film = True

        if request.POST.get('btn_watch') != None:
            film = Film.objects.get(id=film_id)
            film.viewed = True
            film.save()
            context = {
                'films' : new_films_order,
                'only_one_film' : only_one_film,
            }
            return HttpResponseRedirect(f'/films/{film_id}')
        
        elif request.POST.get('btn_viewed') != None:
            film = Film.objects.get(id=film_id)
            film.viewed = False
            film.save()
            context = {
                'films' : new_films_order,
                'only_one_film' : only_one_film,
            }
            return HttpResponseRedirect(f'/films/{film_id}')

        context = {
            'films' : new_films_order,
            'only_one_film' : only_one_film,
        }

        return render(request, 'app_start/films.html', context)
    

def profile(request):
    msg = ''

    current_user = User.objects.get(id=get_current_account_id())

    if request.POST.get('btn_save') != None:
        _login = request.POST.get('login')
        _password = request.POST.get('password')

        if User.objects.filter(login=_login, password=_password).count() != 0 and (_login != current_user.login and _password != current_user.password):
            msg = 'пользователь существует'
        
        else:
            current_user.login = _login
            current_user.password = _password
            current_user.save()
            return HttpResponseRedirect('/films')
    
    elif request.POST.get('btn_exit') != None:
        return HttpResponseRedirect('/')   
    
    context = {
        'login' : current_user.login,
        'password' : current_user.password,
        'msg' : msg,
    }
    return render(request, 'app_start/profile.html', context)


def film_adding(request):
    if request.POST.get('btn_save') != None:
        film = Film()
        film.title = request.POST.get('film_title')
        film.year = request.POST.get('film_year')
        film.genre = request.POST.get('film_genre')
        film.director = request.POST.get('film_director')
        film.discription = request.POST.get('film_discr')
        film.user_id = get_current_account_id()
        film.save()

        if Film.objects.filter(user_id=get_current_account_id()).count() > 1:
            return HttpResponseRedirect(f'/films/{film.id}')  
        else:
            return HttpResponseRedirect('/films')  

    return render(request, 'app_start/film_adding.html')


def film_edit(request, film_id: int):
    film = Film.objects.get(id=film_id)

    if request.POST.get('btn_save') != None:
        film.title = request.POST.get('film_title')
        film.year = request.POST.get('film_year')
        film.genre = request.POST.get('film_genre')
        film.director = request.POST.get('film_director')
        film.discription = request.POST.get('film_discr')
        film.save()
        return HttpResponseRedirect('/films', { 'films' : Film.objects.filter(user_id=get_current_account_id()) })
    
    elif request.POST.get('btn_delete') != None:
        film.delete()
        return HttpResponseRedirect('/films', { 'films' : Film.objects.filter(user_id=get_current_account_id()) })  

    return render(request, 'app_start/film_edit.html', { 'film' : film })
